<?php
require 'aws-autoloader.php';

define("AWS_KEY",           "<key>");
define("AWS_SECRET",        "<secret>");
define("AWS_STORAGECLASS",  "STANDARD"); // STANDARD | REDUCED_REDUNDANCY

use Aws\S3\S3Client;

if((count($argv) !== 3) && (count($argv) !== 4)) {
  echo "Usage php aws-syncdir.php [bucketname] [dirpath] [keyprefix = null]".PHP_EOL;
  die();
}

$bucket = $argv[1];
$dirpath = $argv[2];
$keyprefix = (count($argv) === 3 ? null : $argv[3]);

echo 'Creating AWS client...'.PHP_EOL;
$client = S3Client::factory(array(
      'key'     => AWS_KEY,
      'secret'  => AWS_SECRET
      ));

echo 'Processing directory '.$dirpath.'...'.PHP_EOL;
if(!file_exists($dirpath)) {
  echo "Error processing [$dirpath]; cannot find folder.".PHP_EOL;
  die();
}

$client->uploadDirectory($dirpath, $bucket, $keyprefix, ['debug' => true]);
echo 'Done'.PHP_EOL;
