<?php
require 'aws-autoloader.php';

define("AWS_KEY",           "<key>");
define("AWS_SECRET",        "<secret>");
define("AWS_STORAGECLASS",  "STANDARD"); // STANDARD | REDUCED_REDUNDANCY

use Aws\S3\S3Client;

if(count($argv) < 3) {
  echo "Usage php aws-upload.php [bucketname] [filespattern]".PHP_EOL;
  die();
}

$bucket = $argv[1];
$files = array_slice($argv, 2);

echo 'Creating AWS client...'.PHP_EOL;
$client = S3Client::factory(array(
      'key'     => AWS_KEY,
      'secret'  => AWS_SECRET
      ));

echo 'Processing files ('.count($files).' files)'.PHP_EOL;
foreach($files as $filepath) {
  if(!file_exists($filepath)) {
    echo "Error processing [$filepath]; cannot find file.".PHP_EOL;
    continue;
  }

  $pathinfo = pathinfo($filepath);

  echo 'Putting object '.$pathinfo['basename'].' into '.$bucket.' bucket...'.PHP_EOL;
  $result = $client->putObject(array(
        'Bucket'        => $bucket, 
        'Key'           => $pathinfo['basename'],
        'StorageClass'  => AWS_STORAGECLASS,
        'SourceFile'    => $filepath
        ));

  echo $result.PHP_EOL;
}
