<?php
require 'aws-autoloader.php';

define("AWS_KEY",           "<key>");
define("AWS_SECRET",        "<secret>");
define("AWS_STORAGECLASS",  "STANDARD"); // STANDARD | REDUCED_REDUNDANCY

use Aws\S3\S3Client;

if((count($argv) !== 3) && (count($argv) !== 4)) {
  echo "Usage php aws-downdir.php [destination] [bucketname] [keyprefix = '']".PHP_EOL;
  die();
}

$destination = $argv[1];
$bucket = $argv[2];
$keyprefix = (count($argv) === 3 ? '' : $argv[3]);

echo 'Creating AWS client...'.PHP_EOL;
$client = S3Client::factory(array(
      'key'     => AWS_KEY,
      'secret'  => AWS_SECRET
      ));

echo 'Downloading directory '.$dirpath.'...'.PHP_EOL;
$client->downloadBucket($destination, $bucket, $keyPrefix, ['debug' => true]);
echo 'Done'.PHP_EOL;
